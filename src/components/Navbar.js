import React, { useState, useEffect } from "react"
import { Link } from "react-scroll"
import { Navbar, Nav, Container } from "react-bootstrap"
import { IoIosPaperPlane } from "react-icons/io"
import styled from "styled-components"

const NavbarCustom = styled(Navbar)`
  background: ${({ active }) => (active ? "#0f0f0f" : "transparent")};
  height: 80px;
`

const NavText = styled.span`
  margin-left: 16px;
  font-size: 18px;
`

const Header = () => {
  const [scroll, setScroll] = useState(false)

  const changeNav = () => {
    if (window.scrollY >= 80) {
      setScroll(true)
    } else {
      setScroll(false)
    }
  }

  useEffect(() => {
    changeNav()
    window.addEventListener("scroll", changeNav)
  }, [])

  return (
    <NavbarCustom expand="lg" variant="dark" sticky="top" active={scroll}>
      <Container>
        <Navbar.Brand href="/">
          <IoIosPaperPlane size={28} className="mb-1" />
          <span className="ml-2" style={{ fontSize: "24px" }}>
            KurangPiknik
          </span>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarResponsive" />
        <Navbar.Collapse id="navbarResponsive">
          <Nav as="ul" className="ml-auto">
            <Nav.Item as="li">
              <Link
                to="destination"
                spy={true}
                smooth={true}
                className="nav-link"
              >
                <NavText>Destinations</NavText>
              </Link>
            </Nav.Item>
            <Nav.Item as="li">
              <Link to="whyUs" spy={true} smooth={true} className="nav-link">
                <NavText>Why Us</NavText>
              </Link>
            </Nav.Item>
            <Nav.Item as="li">
              <Link to="booking" spy={true} smooth={true} className="nav-link">
                <NavText>Booking</NavText>
              </Link>
            </Nav.Item>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </NavbarCustom>
  )
}

export default Header
