import React from "react"
import Navbar from "./Navbar"
import Footer from "./Footer"

import styled from "styled-components"
import "bootstrap/dist/css/bootstrap.min.css"

const ContentWrapper = styled.main`
  min-height: 3750px;
`

const Layout = ({ children }) => {
  return (
    <>
      <Navbar />
      <ContentWrapper>{children}</ContentWrapper>
      <Footer />
    </>
  )
}

export default Layout
