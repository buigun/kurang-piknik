import React, { useState } from "react"

import {
  Container,
  Navbar,
  Row,
  Col,
  InputGroup,
  Form,
  FormControl,
  Button,
} from "react-bootstrap"

import { FaTwitter, FaYoutube, FaFacebook, FaInstagram } from "react-icons/fa"
import { IoIosPaperPlane } from "react-icons/io"
import styled from "styled-components"

export const SocialIconLink = styled.a`
  color: #fff;
  font-size: 24px;

  :hover {
    color: grey;
  }
`

const Footer = () => {
  const [email, setEmail] = useState("")

  const handleSubmit = e => {
    e.preventDefault()
    console.log(email)
    setEmail("")
  }

  return (
    <>
      <Navbar
        expand="lg"
        variant="dark"
        style={{
          background: "#0f0f0f",
          color: "white",
          padding: "3em",
        }}
      >
        <Container>
          <Row style={{ minWidth: "100%" }}>
            <Col md={{ span: 4, offset: 4, order: 2 }}>
              <div className="subscription-footer mt-3">
                <h4>LOVE DISCOUNT?</h4>
                <InputGroup className="mb-3 mt-3">
                  <FormControl
                    type="email"
                    placeholder="Enter email"
                    name="email"
                    value={email}
                    onChange={event => setEmail(event.target.value)}
                  />
                  <InputGroup.Append>
                    <Button variant="dark" onClick={handleSubmit}>
                      Subscribe
                    </Button>
                  </InputGroup.Append>
                  <Form.Text className="text-muted mt-2">
                    <p
                      style={{
                        color: "white",
                        fontWeight: 400,
                        textShadow: "1px 1px 1px rgba(0, 0, 0, 0.7)",
                      }}
                    >
                      Subscribe to receive special offer and discount
                    </p>
                  </Form.Text>
                </InputGroup>
              </div>
              <div className="socmed-footer">
                <h4>GET TO KNOW US</h4>
                <Row mt-3>
                  <Col>
                    <SocialIconLink href="/">
                      <FaTwitter />
                    </SocialIconLink>
                  </Col>
                  <Col>
                    <SocialIconLink href="/">
                      <FaYoutube />
                    </SocialIconLink>
                  </Col>
                  <Col>
                    <SocialIconLink href="/">
                      <FaFacebook />
                    </SocialIconLink>
                  </Col>
                  <Col>
                    <SocialIconLink href="/">
                      <FaInstagram />
                    </SocialIconLink>
                  </Col>
                </Row>
              </div>
            </Col>
            <Col md={(4, { order: 1 })}>
              <div className="logo-footer mt-4 mb-3">
                <IoIosPaperPlane size={28} className="mb-1" />
                <span className="ml-2" style={{ fontSize: "24px" }}>
                  KurangPiknik
                </span>
              </div>
              <div className="contact-footer mt-2">
                <h4>Contact</h4>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Magnam, quia.
                </p>
                <p>Lorem ipsum dolor sit amet.</p>
              </div>
              <span>KurangPiknik © {new Date().getFullYear()}</span>
            </Col>
          </Row>
        </Container>
      </Navbar>
    </>
  )
}

export default Footer
