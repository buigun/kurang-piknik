import React from "react"
import { qoreContext } from "../contexts/qoreContext"

const Component = () => {
  const { data, status } = qoreContext.views.allDestinations.useListRow({})
  //   console.log(data)

  return <div>{data && data.map(task => <li>{task.name}</li>)}</div>
}

export default Component
