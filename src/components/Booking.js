import React from "react"
import { Form, Row, Col, Button, Container } from "react-bootstrap"
import styled from "styled-components"
import BgImage from "../images/background2.jpg"

const Section = styled.div`
  position: relative;
  height: 100vh;
  background: linear-gradient(
      180deg,
      rgba(0, 0, 0, 0.5) 0%,
      rgba(0, 0, 0, 0.5) 35%,
      rgba(0, 0, 0, 0.1) 100%
    ),
    url(${BgImage}) no-repeat center;
  background-size: cover;
`

const SectionCenter = styled.div`
  position: absolute;
  top: 50%;
  left: 0;
  right: 0;
  -webkit-transform: translateY(-50%);
  transform: translateY(-50%);
`

const BookingForm = styled.div`
  position: relative;
  max-width: 642px;
  width: 100%;
  margin: auto;
  background: #fff;
  -webkit-box-shadow: 0px 5px 10px -5px rgba(0, 0, 0, 0.3);
  box-shadow: 0px 5px 10px -5px rgba(0, 0, 0, 0.3);
`

const FormCustom = styled(Form)`
  padding: 28px;
  border: 1px solid #f9fafc;
  border-left: 0px;
`

const FormHeader = styled.div`
  margin-bottom: 30px;

  h2 {
    margin: 0;
    font-weight: 700;
    color: #122244;
    font-size: 24px;
  }
`

const FormGroupCustom = styled(Form.Group)`
  position: relative;
  margin-bottom: 16px;
`

const ButtonCustom = styled(Button)`
  color: #fff;
  background-color: #6499ff;
  font-weight: 700;
  padding: 13px 35px;
  font-size: 16px;
  border: none;
  width: 100%;
`

const Booking = () => {
  return (
    <Section>
      <SectionCenter>
        <Container>
          <BookingForm>
            <FormCustom>
              <FormHeader>
                <h2>Booking Now</h2>
              </FormHeader>
              <Row>
                <Col className="md-6">
                  <FormGroupCustom>
                    <Form.Label>First Day</Form.Label>
                    <Form.Control type="date" required />
                  </FormGroupCustom>
                </Col>
                <Col className="md-6">
                  <FormGroupCustom>
                    <Form.Label>Last Day</Form.Label>
                    <Form.Control type="date" required />
                  </FormGroupCustom>
                </Col>
              </Row>
              <FormGroupCustom>
                <Form.Label>Destination</Form.Label>
                <Form.Control as="select">
                  <option>Bali, Indonesia</option>
                  <option>Taj Mahal, India</option>
                  <option>Cairo, Egypt</option>
                  <option>Tokyo, Japan</option>
                  <option>Paris, France</option>
                </Form.Control>
              </FormGroupCustom>
              <FormGroupCustom>
                <Form.Label>Guest</Form.Label>
                <Form.Control as="select">
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                </Form.Control>
              </FormGroupCustom>
              <FormGroupCustom>
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter your email"
                  required
                />
              </FormGroupCustom>
              <FormGroupCustom>
                <Form.Label>Phone</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter your phone number"
                  required
                />
              </FormGroupCustom>
              <ButtonCustom type="submit" variant="primary">
                Book Now
              </ButtonCustom>
            </FormCustom>
          </BookingForm>
        </Container>
      </SectionCenter>
    </Section>
  )
}

export default Booking
