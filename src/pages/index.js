import React from "react"
import { Element } from "react-scroll"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Hero from "../components/Hero"
import Destinations from "../components/Destinations"
import Stats from "../components/Stats"
import Testimonials from "../components/Testimonials"
import Booking from "../components/Booking"
// import Component from "../components/Component"

// import qoreContext from "../contexts/qoreContext"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <Hero />
    <Element id="destination" name="destination">
      <Destinations />
    </Element>
    <Element id="whyUs" name="whyUs">
      <Stats />
      <Testimonials />
    </Element>
    {/* <Component /> */}
    <Element id="booking" name="booking" className="mt-5">
      <Booking />
    </Element>
  </Layout>
)

export default IndexPage
